//Libreria de Mbed
#include "mbed.h"

//Direccion del Esclavo
#define     MPU6050_address             0xD0 //Direccion de 7 bit I2C de la MPU 6050

//Escalas (Giroscopio)
#define     GYRO_FULL_SCALE_250_DPS     0x00
#define     GYRO_FULL_SCALE_500_DPS     0x08
#define     GYRO_FULL_SCALE_1000_DPS    0x10
#define     GYRO_FULL_SCALE_2000_DPS    0x18

//Escalas (Acelerometro)
#define     ACC_FULL_SCALE_2_G          0x00
#define     ACC_FULL_SCALE_4_G          0x08
#define     ACC_FULL_SCALE_8_G          0x10
#define     ACC_FULL_SCALE_16_G         0x18

//Escalas de conversion especificas de la documentacion de la MPU6050 segun la escalizacion de 16 bits
#define SENSITIVITY_ACCEL       2.0/32768.0             //Valor de conversion del acelerometro
#define SENSITIVITY_GYRO        250.0/32768.0           //Valor de conversion del giroscopio
#define SENSITIVITY_TEMP        333.87                  //Valor de sensibilidad de temperatura
#define TEMP_OFFSET             21                      //Valor de compensacion de temperatura
#define SENSITIVITY_MAGN        (10.0*4800.0)/32768.0   //Valor de conversion del magnetometro

//Decalaracion de variables

//Valores "RAW" de tipo entero
int16_t raw_accelx, raw_accely, raw_accelz;
int16_t raw_gyrox, raw_gyroy, raw_gyroz;
int16_t raw_temp;

float offset_accelx = 0.0, offset_accely = 0.0, offset_accelz = 0.0;
float offset_gyrox = 0.0, offset_gyroy = 0.0, offset_gyroz = 0.0;

float max_accelx = -32768.0, min_accelx = 32767.0, max_accely = -32768.0, min_accely = 32767.0, max_accelz = -32768.0, min_accelz = 32767.0;
float max_gyrox = -32768.0, min_gyrox = 32767.0, max_gyroy = -32768.0, min_gyroy = 32767.0,max_gyroz = -32768.0, min_gyroz = 32767.0;

float accelx, accely, accelz;
float gyrox, gyroy, gyroz;
float temp;

//Bytes
char cmd[2];
char data[1];
char GirAcel[14];

float buffer[500][8];
int i, offset_samples=100;
Timer t; // Definicion del objeto temporizador
float timer=0;

//Definicion del objeto de comunicacion serial
Serial pc (USBTX, USBRX);
//Definicion del objeto I2C
I2C i2c (PB_9, PB_8);//SDA, SCL

//Programa principal donde va a declarar todas las acciones previas a ejecutar en el programa
int main()
{
    //Desactiva el modo de hibernacion de la MPU6050
    cmd[0] = 0x6B;
    cmd[1] = 0x00;
    i2c.write(MPU6050_address, cmd, 2);

    pc.printf("PRUEBA DE CONEXION DEL GIROSCOPIO Y ACELEROMETRO \n\r");
    //
    //¿Quién soy yo para el mpu6050?
    //
    pc.printf("1. prueba de conexion de MPU6050... \n\r");//Verificacion de la conexion
    cmd[0] = 0x75;
    i2c.write(MPU6050_address, cmd, 1);
    i2c.read(MPU6050_address, data, 1);
    if (data[0] !=0x68) {   //Valor por defecto de registro
        pc.printf("Error de conexion con la MPU6050 \n\r");
        pc.printf("Ups, no soy la MPU6050, ¿quien soy? :S. Soy: %#x \n\r",data[0]);
        pc.printf("\n\r");
        while (1);
        //Si no hay error la conexion es exitosa
    } else {
        pc.printf("Conexion exitosa con la MPU6050 \n\r");
        pc.printf("Hola, ¿todo bien?... soy la MPU6050 XD \n\r");
        pc.printf("\n\r");
    }
    wait(0.1);
    //Configuracion del giroscopio
    cmd[0] = 0x1B;  //GYRO_CONFIG 0x1B
    cmd[1] = 0x00;
    i2c.write(MPU6050_address, cmd, 2);
    //Configuracion del acelerometro
    cmd[0] = 0x1C;  //ACCEL_CONFIG 0x1C
    cmd[1] = 0x00;
    i2c.write(MPU6050_address, cmd, 2);
    wait (0.01);
    //Ciclo infinito del programa
    while(1) {
        pc.printf("CALIBRACIÓN DE GIRÓMETRO Y ACELERÓMETRO \n\r");
        pc.printf("no los toque hasta que lea el mensaje de 'calibración finalizada' \n\r" );
        wait(2);
        pc.printf("coloque la imagen en posición horizontal con las letras hacia arriba \n\r");
        wait(2);
        pc.printf("eyendo los sensores por primera vez ... \n\r");
        for (i=0; i<offset_samples; i++) {
            cmd[0]=0x3B;
            i2c.write(MPU6050_address, cmd, 1);
            i2c.read(MPU6050_address, GirAcel, 14);
            raw_accelx = GirAcel[0]<<8 | GirAcel[1];
            raw_accely = GirAcel[2]<<8 | GirAcel[3];
            raw_accelz = GirAcel[4]<<8 | GirAcel[5];
            raw_temp = GirAcel[6]<<8 |  GirAcel[7];
            raw_gyrox = GirAcel[8]<<8 | GirAcel[9];
            raw_gyroy = GirAcel[10]<<8 | GirAcel[11];
            raw_gyrox = GirAcel[12]<<8 | GirAcel[13];
            wait(0.01);
        }
        pc.printf("Leyendo los sensores y calculando las compensaciones... \n\r");
        for (i=0; i<offset_samples; i++) {
            cmd[0]=0x3B;
            i2c.write(MPU6050_address, cmd, 1);
            i2c.read(MPU6050_address, GirAcel, 14);
            raw_accelx = GirAcel[0]<<8 | GirAcel[1];
            raw_accely = GirAcel[2]<<8 | GirAcel[3];
            raw_accelz = GirAcel[4]<<8 | GirAcel[5];
            raw_temp = GirAcel[6]<<8 |  GirAcel[7];
            raw_gyrox = GirAcel[8]<<8 | GirAcel[9];
            raw_gyroy = GirAcel[10]<<8 | GirAcel[11];
            raw_gyrox = GirAcel[12]<<8 | GirAcel[13];

            if (raw_accelx >= max_accelx) {
                max_accelx = raw_accelx;
            }
            if (raw_accelx >= min_accelx) {
                min_accelx = raw_accelx;
            }

            if (raw_accely >= max_accely) {
                max_accely = raw_accely;
            }
            if (raw_accely >= min_accely) {
                min_accely = raw_accely;
            }

            if (raw_accelz >= max_accelz) {
                max_accelz = raw_accelz;
            }
            if (raw_accelz >= min_accelz) {
                min_accelz = raw_accelz;
            }


            if (raw_gyrox >= max_gyrox) {
                max_gyrox = raw_gyrox;
            }
            if (raw_gyrox >= min_gyrox) {
                min_gyrox = raw_gyrox;
            }

            if (raw_gyroy >= max_gyroy) {
                max_gyroy = raw_gyroy;
            }
            if (raw_gyroy >= min_gyroy) {
                min_gyroy = raw_gyroy;
            }

            if (raw_gyroz >= max_gyroz) {
                max_gyroz = raw_gyroz;
            }
            if (raw_gyroz >= min_gyroz) {
                min_gyroz = raw_gyroz;
            }
            wait(0.01);
        }
        offset_accelx = (max_accelx + min_accelx) / 2;
        offset_accely = (max_accely + min_accely) / 2;
        offset_accelz = (max_accelz + min_accelz) / 2;

        offset_gyrox = (max_gyrox + min_gyrox) / 2;
        offset_gyroy = (max_gyroy + min_gyroy) / 2;
        offset_gyroz = (max_gyroz + min_gyroz) / 2;
        pc.printf("Calibracion finalizada de acelerometro y diroscopio \n\r");
        pc.printf("offset_accelx = %.2f, offset_accely = %.2f, offset_accelz = %.2f, offset_gyrox = %.2f, offset_gyroy = %.2f, offset_gyroz = %.2f \n\r", offset_accelx, offset_accely, offset_accelz, offset_gyrox, offset_gyroy, offset_gyroz);
        pc.printf("CALIBRACION ENCERRADA \n\r");
        wait(100);
    }
}
